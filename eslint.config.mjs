// @ts-check
import antfu, { perfectionist } from '@antfu/eslint-config';

import withNuxt from './.nuxt/eslint.config.mjs';

export default withNuxt(
  await antfu({
    plugins: [perfectionist()],
    unocss: { strict: true, attributify: true },
    stylistic: {
      overrides: {
        'style/semi': 'error',
        'style/max-len': ['error', 120],
        'style/member-delimiter-style': 'error',
        'style/array-bracket-newline': ['error', { multiline: true }],
        'style/object-curly-newline': ['error', { multiline: true, consistent: true }],
      },
    },
    rules: {
      'sort-imports': 'off',
      'func-style': ['error', 'expression'],

      'import/order': 'off',

      'antfu/if-newline': 'off',
      'antfu/top-level-function': 'off',

      'unused-imports/no-unused-imports': 'error',

      'vue/no-multiple-template-root': 'off',
      'vue/multi-word-component-names': 'off',
      'vue/no-empty-component-block': 'error',
      'vue/block-lang': ['error', { script: { lang: 'ts' } }],
      'vue/block-order': ['error', { order: ['script', 'template', 'style'] }],

      'perfectionist/sort-imports': ['error', { type: 'line-length' }],
      'perfectionist/sort-exports': ['error', { type: 'line-length' }],
      'perfectionist/sort-objects': ['error', { type: 'line-length', partitionByNewLine: true }],
      'perfectionist/sort-interfaces': ['error', { type: 'line-length', partitionByNewLine: true }],
      'perfectionist/sort-named-imports': ['error', { type: 'line-length', groupKind: 'values-first' }],
      'perfectionist/sort-named-exports': ['error', { type: 'line-length', groupKind: 'values-first' }],
    },
  }),
);
