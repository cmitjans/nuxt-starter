// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  unocss: { preflight: true },
  devtools: { enabled: true },
  modules: ['@nuxt/eslint', '@unocss/nuxt'],
  eslint: { config: { standalone: false } },
});
